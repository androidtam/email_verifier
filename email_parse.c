#include <stdio.h>
#include <stdlib.h>
#include "email_parse.h"

int email_parse(char *email)
{
    if (!email)
    {
        printf("Please add an email address!\n");
        return EXIT_FAILURE;
    }
    printf("Your email is: %s\n", email);
    char first = email[0];
    if ( first == '.' || first == '@' )
    {
        printf("Wrong mail format, first character cannot be '%c'!", first);
        return EXIT_FAILURE;

    }
    int i = 0;
    int at_sign = 0;
    int dot_sign = 0;
    int dot_after_at = 0;
    while ( email[i] )
    {
        if (email[i] == '@')
        {
          at_sign++;
        }
        if ( at_sign > 1 )
        {
            printf("Wrong email format, too many '@' characters: %d!", at_sign);
            return EXIT_FAILURE;
        }
        if (email[i] == '.')
        {
          dot_sign++;
        }
        if ((email[i] == '.' && email[i] == email[i-1]) || (email[i] == '.' && email[i-1] == '@') || (email[i] == '@' && email[i-1] == '.'))
        {
            printf("Wrong email format: Two '.', or a .' and a '@' cannot be beside each other!");
            return EXIT_FAILURE;
        }
        if (at_sign == 1 && email[i] == '.')
        {
            dot_after_at++;
        }
        i++;
    }
    if (email[i-1] == '.' || email[i-1] == '@')
    {
        printf("Wrong mail format, last character cannot be '%c'!", email[i-1]);
        return EXIT_FAILURE;
    }
    if ( at_sign < 1 || dot_sign < 1)
    {
        printf("Wrong email format, a '.' or a '@' character is missing!");
            return EXIT_FAILURE;
    }

    if (dot_after_at != 1)
    {
        printf("Wrong mail format: there must be a '.' after an @, but only 1!");
        return EXIT_FAILURE;
    }

    printf("Proper email format!");
    return EXIT_SUCCESS;
}
