#include <stdio.h>
#include <stdlib.h>
#include "email_parse.h"

int main(int argc, char *argv[])
{
    if (argc == 1)
    {
        printf("Please add an email address!\n");
        return EXIT_FAILURE;
    }
    return email_parse(argv[1]);
}
